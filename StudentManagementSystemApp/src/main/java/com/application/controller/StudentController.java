package com.application.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.application.Entity.Student;
import com.application.service.StudentService;

@Controller
public class StudentController 
{
	private StudentService studentService;

	public StudentController(StudentService studentService) {
		super();
		this.studentService = studentService;
	}
@GetMapping("/students")	
public String listOfStudents(Model model)
{
	model.addAttribute("students",studentService.getAllStudent());
	return "students";
	
}
@GetMapping("/students/new")
public String createStudentForm(Model model)
{
	//to add newly created student
	Student student=new Student();
	model.addAttribute("student", student);
	return "create_student";
	
}
@PostMapping("/students")
public String saveStudent(@ModelAttribute("student") Student student)
{
	studentService.saveStudent(student);
	return "redirect:/students";
	
}
@GetMapping("/students/edit{id}")
public String editForm(@PathVariable Long id, Model model)
{
	model.addAttribute("student", studentService.getStudentById(id));
	return "edit_student";
	
}
@PostMapping("/students/{id}")
public String UpdateStudent(@PathVariable Long id,@ModelAttribute("student") Student student, Model model)
{
	//get student from database by id
	Student existingStudent =studentService.getStudentById(id);
	existingStudent.setId(id);
	existingStudent.setFirstName(student.getFirstName());
	existingStudent.setLastName(student.getLastName());
	existingStudent.setEmail(student.getEmail());
	
	
	//save updated student
	studentService.updateStudent(existingStudent);
	return "redirect:/students";
	
}
//handler method to handle delete student
@GetMapping("/students/remove{id}")	
public String deleteStudent(@PathVariable Long id)
{
	studentService.deleteStudentById(id);
	return "redirect:/students";
	
}
}

