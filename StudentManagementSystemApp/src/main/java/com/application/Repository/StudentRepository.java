package com.application.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.application.Entity.Student;

public interface StudentRepository extends JpaRepository<Student,Long>{

}
