package com.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.application.Entity.Student;
import com.application.Repository.StudentRepository;

@SpringBootApplication
public class StudentManagementSystemAppApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(StudentManagementSystemAppApplication.class, args);
	}
	@Autowired
	private StudentRepository studentRepository;

	@Override
	public void run(String... args) throws Exception {
		/*
		 * Student student1=new Student("Pratik", "Shinde","PratikShinde@gmail.com");
		 * studentRepository.save(student1); Student student2=new Student("Avi",
		 * "Rane","AviRane@gmail.com"); studentRepository.save(student2);
		 */
	}

}
